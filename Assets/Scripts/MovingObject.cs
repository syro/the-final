﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {

    public float moveTime = 0.1f;

    private BoxCollider2D boxCollider;
    private Rigidbody2D rigidBody;
    private LayerMask collisionLayer;
    private float inverseMoveTime;


	protected virtual void Start () {
        boxCollider = GetComponent<BoxCollider2D>();
        rigidBody = GetComponent<Rigidbody2D>();
        collisionLayer = LayerMask.GetMask("Colliion Layer");
        inverseMoveTime = 1.0f / moveTime;
	}
	
    protected bool CanObjectMove(int xDirection, int yDirection)
    {
        Vector2 startPosition = rigidBody.position;
        Vector2 endPosition = startPosition + new Vector2(xDirection, yDirection);

        boxCollider.enabled = false;
        RaycastHit2D hit = Physics2D.Linecast(startPosition, endPosition, collisionLayer);
        boxCollider.enabled = true;

        if(hit.transform == null)
        {
            StartCoroutine(SmoothMoveRoutine(endPosition));
            return true;
        }

        return false;
        
    }

    protected IEnumerator SmoothMoveRoutine(Vector2 endPostion)
    {
        float remainingDistanceToEndPostion;

        do
        {
            remainingDistanceToEndPostion = (rigidBody.position - endPostion).sqrMagnitude;
            Vector2 updatedPostion = Vector2.MoveTowards(rigidBody.position, endPostion, inverseMoveTime * Time.deltaTime);
            rigidBody.MovePosition(updatedPostion);
            yield return null;
        } while (remainingDistanceToEndPostion > float.Epsilon);
    }

	void Update () {
	
	}
}
