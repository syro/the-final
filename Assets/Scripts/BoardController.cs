﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

 public class BoardController : MonoBehaviour {

    public int columns;
    public int rows;

    public GameObject[] floors;
    public GameObject[] outerWall;
    public GameObject[] enemies;
    public GameObject exit;
    public GameObject[] foodItems;

    private Transform gameBoard;
    private List<Vector3> obstaclesGrid;

	void Awake () {
        obstaclesGrid = new List<Vector3>();
	}
	
	void Update () {
	
	}

    private void InitializeObstaclePostion()
    {
        obstaclesGrid.Clear();
        for(int x = 2; x < columns - 2; x++)
        {
            for(int y = 2; y < rows - 2; y++)
            {
                obstaclesGrid.Add(new Vector3(x, y, 0f));
            }
        }
    }

    private void SetupGameBoard()
    {
        gameBoard = new GameObject("Game Board").transform;

        for(int x = 0; x < columns; x++)
        {
            for(int y= 0; y < rows; y++)
            {
                GameObject selectedTile;
                if(x == 0 || y == 0 || x == columns - 1  || y == rows - 1)
                {
                    selectedTile = outerWall[Random.Range(0, outerWall.Length)];
                }
                else
                {
                    selectedTile = floors[Random.Range(0, floors.Length)];
                }
 
                GameObject floorTile = (GameObject)Instantiate(selectedTile, new Vector3(x, y, 0f), Quaternion.identity);
                floorTile.transform.SetParent(gameBoard);
            }
        }
    }

    private void SetRandomObstaclesOnGrid(GameObject[] obstaclesArray, int minimum, int maximum)
    {
        int obstacleCount = Random.Range(minimum, maximum + 1);

        if (obstacleCount > obstaclesGrid.Count)
        {
            obstacleCount = obstaclesGrid.Count;
        }

        for(int index = 0; index < obstacleCount; index++)
        {
            
        }
    }

    public void SetupLevel()
    {
        InitializeObstaclePostion();
        SetupGameBoard();
        SetRandomObstaclesOnGrid(enemies, 1, 4);
        SetRandomObstaclesOnGrid(foodItems, 1, 3);
        Instantiate(exit, new Vector3(columns - 2, rows - 2, 0f), Quaternion.identity);
    }
}
